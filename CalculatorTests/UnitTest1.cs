﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorTDD;

namespace CalculatorTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestSum()
        {
            double result = Program.Sum(5,2);

            if(result != 7)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void TestDeduct()
        {
            double result = Program.Deduct(5, 2);

            if (result != 3)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Divide()
        {
            double result = Program.Divide(10, 2);

            if (result != 5)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Multiply()
        {
            double result = Program.Multiply(10, 2);

            if (result != 20)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void SquareRoot()
        {
            double result = Program.SquareRoot(100);

            if (result != 10)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Sin()
        {
            double result = Program.Sin(90);

            if (result != 0.89399665594100952)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Cosine()
        {
            double result = Program.Cosine(30);

            if (result != 0.15425145626068115)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Tangent()
        {
            double result = Program.Tangent(30);

            if (result != -6.4053311347961426)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Log()
        {
            double result = Program.Log(30);

            if (result != 1.4771212339401245)
            {
                throw new Exception();
            }

        }

        [TestMethod]
        public void Pot()
        {
            double result = Program.Pot(2,2);

            if (result != 4)
            {
                throw new Exception();
            }

        }


    }
}
