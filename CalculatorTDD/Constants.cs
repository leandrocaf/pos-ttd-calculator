﻿namespace CalculatorTDD
{
    class Constants
    {
        public static string START_CALC = "Calculadora iniciada...";

        public static string CHOOSE_OPERATION = "Escolha umas das operações possíveis:\n";
        public static string OPERATION_SUM = "Somar";
        public static string OPERATION_DEDUCT = "Subtrair";
        public static string OPERATION_DIVIDE = "Dividir";
        public static string OPERATION_MULTIPLY = "Multiplicar";
        public static string OPERATION_SQUARE_ROOT = "Raiz";
        public static string OPERATION_SIN = "Seno";
        public static string OPERATION_COSINE = "Cosseno";
        public static string OPERATION_TANGENT = "Tangente";
        public static string OPERATION_LOG = "Log";
        public static string OPERATION_POW = "Potencia";

        public static string INVALID_OPERATION = "\nOperação inválida!\n";
        public static string INVALID_VALUE = "Valor inváido!\n";
        public static string ERRO = "Erro\n";
        public static string TRY_AGAIN = "Tentar novamente? [y][n]\n";
        public static string OPTION_YES = "y";
        public static string PROGRAM_END = "Fim do programa!\n";

        public static string RESULT = "Resultado: ";
        public static string FIRST_VALUE = "Entre com o valor para calculo";
        public static string SECOND_VALUE = "Entre com o segundo valor para calculo";
    }
}
