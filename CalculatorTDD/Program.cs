﻿using System;

namespace CalculatorTDD
{
    public class Program
    {
        private static double lastValue;

        static void Main(string[] args)
        {
            Console.WriteLine(Constants.START_CALC);
            ShowOptions();
        }

        //Exibe as opçoes de operaçoes para o usuário
        public static void ShowOptions()
        {
            try
            {

                Console.WriteLine(Constants.CHOOSE_OPERATION);
                Console.WriteLine(Constants.OPERATION_SUM);
                Console.WriteLine(Constants.OPERATION_DEDUCT);
                Console.WriteLine(Constants.OPERATION_DIVIDE);
                Console.WriteLine(Constants.OPERATION_MULTIPLY);
                Console.WriteLine(Constants.OPERATION_SQUARE_ROOT);
                Console.WriteLine(Constants.OPERATION_SIN);
                Console.WriteLine(Constants.OPERATION_COSINE);
                Console.WriteLine(Constants.OPERATION_TANGENT);
                Console.WriteLine(Constants.OPERATION_LOG);
                Console.WriteLine(Constants.OPERATION_POW);
                Console.WriteLine("\n");

                string option = Console.ReadLine();

                if (option == Constants.OPERATION_SUM)
                {
                    ShowResult(Sum(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }
                else if (option == Constants.OPERATION_DEDUCT)
                {
                    ShowResult(Deduct(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }
                else if (option == Constants.OPERATION_DIVIDE)
                {
                    ShowResult(Divide(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }
                else if (option == Constants.OPERATION_DEDUCT)
                {
                    ShowResult(Deduct(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }
                else if (option == Constants.OPERATION_MULTIPLY)
                {
                    ShowResult(Multiply(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }
                else if (option == Constants.OPERATION_SQUARE_ROOT)
                {
                    ShowResult(SquareRoot(RequestUserFirstValue()));
                    return;
                }
                else if (option == Constants.OPERATION_SIN)
                {
                    ShowResult(Sin(RequestUserFirstValue()));
                    return;
                }
                else if (option == Constants.OPERATION_COSINE)
                {
                    ShowResult(Cosine(RequestUserFirstValue()));
                    return;
                }
                else if (option == Constants.OPERATION_TANGENT)
                {
                    ShowResult(Tangent(RequestUserFirstValue()));
                    return;
                }
                else if (option == Constants.OPERATION_LOG)
                {
                    ShowResult(Log(RequestUserFirstValue()));
                    return;
                }
                else if (option == Constants.OPERATION_POW)
                {
                    ShowResult(Pot(RequestUserFirstValue(), RequestUserSecondValue()));
                    return;
                }

                Console.WriteLine(Constants.INVALID_OPERATION);

                TryAgain();

            }
            catch (FormatException)
            {
                Console.WriteLine(Constants.INVALID_VALUE);
            }
            catch (Exception)
            {
                Console.WriteLine(Constants.ERRO);

                TryAgain();
            }
        }

        // Exibe ao usuário a opçao de efetuar uma nova operaçao
        private static void TryAgain()
        {
            Console.WriteLine(Constants.TRY_AGAIN);
            string option = Console.ReadLine();
            if (option == Constants.OPTION_YES)
            {
                ShowOptions();
            }
            else
            {
                Console.WriteLine(Constants.PROGRAM_END);                
            }
        }


        //Exibe o resultado da operaçao
        private static void ShowResult(double result)
        {
            lastValue = result;
            Console.WriteLine(Constants.RESULT + result.ToString() + "\n");
            TryAgain();
        }

        //Solicita o primeiro valor da operacao ao usuário
        private static double RequestUserFirstValue()
        {
            try
            {
                Console.WriteLine(Constants.FIRST_VALUE);
                double value1 = double.Parse(Console.ReadLine());
                return value1;
            }
            catch (Exception)
            {
                throw new InvalidOperationException();
            }
        }

        //Solicita o segundo valor da operaçao ao usuário
        private static double RequestUserSecondValue()
        {
            try
            {
                Console.WriteLine(Constants.SECOND_VALUE);
                double value2 = double.Parse(Console.ReadLine());
                return value2;
            }
            catch (Exception)
            {
                throw new InvalidOperationException();
            }
        }

        //Soma dois valores
        public static double Sum(double value1, double value2)
        {
            return value1 + value2;
        }

        //Subtrair dois valores
        public static double Deduct(double value1, double value2)
        {
            return value1 - value2;
        }
        
        //Divide dois valores
        public static double Divide(double value1, double value2)
        {
            return value1 / value2;
        }

        //Mulitplica dois valores
        public static double Multiply(double value1, double value2)
        {
            return value1 * value2;
        }

        //Raiz quadrada de um número
        public static double SquareRoot(double value)
        {
            return (float)Math.Sqrt(value);
        }

        //Seno
        public static double Sin(double value)
        {
            return (float)Math.Sin(value);
        }

        //Cosseno
        public static double Cosine(double value)
        {
            return (float)Math.Cos(value);
        }

        //Tangente
        public static double Tangent(double value)
        {
            return (float)Math.Tan(value);
        }

        //Logaritimo
        public static double Log(double value)
        {
            return (float)Math.Log10(value);
        }

        //Calcula potencia
        public static double Pot(double value1, double value2)
        {
            return (float)Math.Pow(value1, value2);
        }
    }
}
